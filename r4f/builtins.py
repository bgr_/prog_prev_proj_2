""" Builtins for the r4f language, available through `require r4f`."""


VERSION = 42


def out(*args):
    print(*args)


def crlf():
    print()


def pow(n, p):
    return n ** p
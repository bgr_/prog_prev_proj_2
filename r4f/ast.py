from attr import attr, attributes
import importlib


@attributes
class Program:
    statements = attr()

    def eval(self):
        env = {}
        for st in self.statements:
            st.eval(env)
        return env


@attributes
class Require:
    module_path = attr()

    def eval(self, env):
        pass  # it was already imported during parsing


@attributes
class Assignment:
    name = attr()
    expression = attr()

    def eval(self, env):
        env[self.name] = self.expression.eval(env)


@attributes
class ExprAdd:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) + self.b.eval(env)


@attributes
class ExprSub:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) - self.b.eval(env)


@attributes
class ExprMul:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) * self.b.eval(env)


@attributes
class ExprDiv:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) * self.b.eval(env)


@attributes
class ExprGT:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) > self.b.eval(env)


@attributes
class ExprGE:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) >= self.b.eval(env)


@attributes
class ExprLT:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) < self.b.eval(env)


@attributes
class ExprLE:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) <= self.b.eval(env)


@attributes
class ExprEQ:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) == self.b.eval(env)


@attributes
class ExprNE:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) != self.b.eval(env)


@attributes
class ExprMod:
    a = attr()
    b = attr()

    def eval(self, env):
        return self.a.eval(env) % self.b.eval(env)


@attributes
class Integer:
    value = attr()

    def eval(self, _):
        return self.value


@attributes
class Reference:
    name = attr()

    def eval(self, env):
        return env[self.name]


@attributes
class NSReference:
    path = attr()

    def eval(self, _):
        path = self.path[:]
        if path[0] == 'r4f':
            path = ['r4f', 'builtins'] + path[1:]
        module = importlib.import_module('.'.join(path[:-1]))
        return getattr(module, path[-1])


@attributes
class While:
    condition_expression = attr()
    statements = attr()

    def eval(self, env):
        while self.condition_expression.eval(env):
            for st in self.statements:
                st.eval(env)


@attributes
class If:
    condition_expression = attr()
    statements = attr()

    def eval(self, env):
        if self.condition_expression.eval(env):
            for st in self.statements:
                st.eval(env)


@attributes
class For:
    varname = attr()
    start = attr()
    stop = attr()
    step = attr()
    statements = attr()

    def eval(self, env):
        old_var_value = env.get(self.varname, None)

        for i in range(self.start, self.stop, self.step):
            env[self.varname] = i
            for st in self.statements:
                st.eval(env)

        if old_var_value is not None:
            env[self.varname] = old_var_value
        else:
            del env[self.varname]


@attributes
class FunctionCall:
    ref = attr()
    args = attr()

    def eval(self, env):
        ref = self.ref.eval(env)
        args = [expr.eval(env) for expr in self.args]
        return ref(*args)
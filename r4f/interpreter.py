""" Interprets the r4f source file given via command-line argument."""

if __name__ == "__main__":
    import sys
    with open(sys.argv[1]) as f:
        src = f.read()

    # disable warnings
    import logging
    logger = logging.getLogger('ply')
    logger.setLevel(logging.ERROR)

    from r4f.parsing import parse
    result, _ = parse(src)
    env = result.eval()

import importlib
import logging
from ply import yacc
from r4f.tokens import tokens  # noqa: import is required by yacc
from r4f.ast import Program, Assignment, ExprAdd, ExprSub, ExprMul, ExprDiv, \
    Integer, Reference, Require, While, If, ExprGT, ExprGE, ExprLT, ExprLE, \
    ExprEQ, ExprNE, ExprMod, For, NSReference, FunctionCall
import r4f.builtins  # noqa: required for find_loader to pass for some reason

_defined_symbols = None  # will be re-assigned on each parse

start = 'program'


def p_program(p):
    "program : requires statements"
    requires, statements = p[1], p[2]
    assert isinstance(requires, list)
    assert isinstance(statements, list)
    p[0] = Program(requires + statements)


def p_requires_multiple(p):
    "requires : requires require"
    p[0] = p[1] + [p[2]]


def p_requires_single(p):
    "requires : require"
    p[0] = [p[1]]


def p_requires_empty(p):
    "requires : empty"
    p[0] = []


def p_require(p):
    "require : REQUIRE idents DOT"
    idents = p[2]
    original_idents = idents[:]

    if idents == ['r4f']:
        # redirect builtins module
        idents = ['r4f', 'builtins']

    py_name = '.'.join(idents)
    if importlib.find_loader(py_name) is None:
        raise ImportError("Can't import '{}'".format(' '.join(idents)))
    else:
        _defined_symbols.add(py_name)
        p[0] = Require(original_idents)


def p_idents_multiple(p):
    "idents : idents IDENT"
    p[0] = p[1] + [p[2]]


def p_idents_single(p):
    "idents : IDENT"
    p[0] = [p[1]]


def p_statements_multiple(p):
    "statements : statements statement"
    p[0] = p[1] + [p[2]]


def p_statements_single(p):
    "statements : statement"
    p[0] = [p[1]]


def p_statements_empty(p):
    "statements : empty"
    p[0] = []


def p_statement(p):
    """statement : define
                 | assign
                 | while
                 | if
                 | for
                 | expr DOT
    """
    p[0] = p[1]


def p_define(p):
    "define : DEF IDENT ASSIGN expr DOT"
    name, expr = p[2], p[4]
    p[0] = Assignment(name, expr)
    _defined_symbols.add(name)


def p_assign(p):
    "assign : IDENT ASSIGN expr DOT"
    name, expr = p[1], p[3]
    if name not in _defined_symbols:
        raise ReferenceError("Symbol '{}' must be defined first".format(name))
    else:
        p[0] = Assignment(name, expr)


def p_expr_plus(p):
    "expr : expr PLUS term"
    p[0] = ExprAdd(p[1], p[3])


def p_expr_minus(p):
    "expr : expr MINUS term"
    p[0] = ExprSub(p[1], p[3])


def p_expr_gt(p):
    "expr : expr GT term"
    p[0] = ExprGT(p[1], p[3])


def p_expr_ge(p):
    "expr : expr GE term"
    p[0] = ExprGE(p[1], p[3])


def p_expr_lt(p):
    "expr : expr LT term"
    p[0] = ExprLT(p[1], p[3])


def p_expr_LE(p):
    "expr : expr LE term"
    p[0] = ExprLE(p[1], p[3])


def p_expr_eq(p):
    "expr : expr EQ term"
    p[0] = ExprEQ(p[1], p[3])


def p_expr_ne(p):
    "expr : expr NE term"
    p[0] = ExprNE(p[1], p[3])


def p_expr_mod(p):
    "expr : expr MOD term"
    p[0] = ExprMod(p[1], p[3])


def p_expr_term(p):
    "expr : term"
    p[0] = p[1]


def p_term_mul(p):
    "term : term MUL factor"
    p[0] = ExprMul(p[1], p[3])


def p_term_div(p):
    "term : term DIV factor"
    p[0] = ExprDiv(p[1], p[3])


def p_term_factor(p):
    "term : factor"
    p[0] = p[1]


def p_factor_number(p):
    "factor : INTEGER"
    p[0] = Integer(int(p[1]))


def p_factor_ident(p):
    "factor : IDENT"
    name = p[1]
    if name not in _defined_symbols:
        raise ReferenceError("Undefined symbol '{}'".format(name))
    p[0] = Reference(name)


def p_factor_namespace_reference(p):
    "factor : nsreference"
    p[0] = p[1]


def p_namespace_reference(p):
    "nsreference : nsreference_unchecked"
    path = p[1]
    original_path = path[:]

    if path[0] == 'r4f':
        path = ['r4f', 'builtins'] + path[1:]

    py_name = '.'.join(path[:-1])  # exclude last element, looking up parent
    r4f_name = ' '.join(path[:-1])

    if py_name not in _defined_symbols:
        raise ReferenceError("Missing 'require' for '{}'".format(r4f_name))

    module = importlib.import_module(py_name)

    if path[-1] not in dir(module):
        raise ReferenceError("'{}' not found in module '{}'".format(
            path[-1], r4f_name))

    p[0] = NSReference(original_path)


def p_namespace_reference_multiple(p):
    "nsreference_unchecked : IDENT COLON nsreference_unchecked"
    p[0] = [p[1]] + p[3]


def p_namespace_reference_single(p):
    "nsreference_unchecked : IDENT"
    p[0] = [p[1]]


def p_factor_expr(p):
    "factor : LPAREN expr RPAREN"
    p[0] = p[2]


def p_factor_function_call(p):
    "factor : nsreference LPAREN exprs RPAREN"
    ref, args = p[1], p[3]
    p[0] = FunctionCall(ref, args)


def p_exprs_multiple(p):
    "exprs : exprs COMMA expr"
    p[0] = p[1] + [p[3]]


def p_exprs_single(p):
    "exprs : expr"
    p[0] = [p[1]]


def p_exprs_empty(p):
    "exprs : empty"
    p[0] = []


def p_while(p):
    "while : WHILE expr DO statements STOP"
    condition, statements = p[2], p[4]
    p[0] = While(condition, statements)


def p_if(p):
    "if : IF expr DO statements STOP"
    condition, statements = p[2], p[4]
    p[0] = If(condition, statements)


def p_for(p):
    "for : FOR IDENT for_define ASSIGN INTEGER TO INTEGER WITH STEP INTEGER DO statements STOP for_undefine"  # noqa
    varname, start, stop, step, statements = p[2], p[5], p[7], p[10], p[12]
    start, stop, step = int(start), int(stop), int(step)
    if step == 0:
        raise ValueError("Iteration step must be non-zero")
    p[0] = For(varname, int(start), int(stop), int(step), statements)


def p_for_define(p):
    "for_define :"
    name = p[-1]
    already_defined = name in _defined_symbols
    _defined_symbols.add(name)
    p[0] = (name, already_defined)


def p_for_undefine(p):
    "for_undefine :"
    name, was_defined = p[-11]
    if not was_defined:
        _defined_symbols.remove(name)


def p_empty(p):
    "empty :"
    pass


def p_error(p):
    raise SyntaxError("Syntax error at line {}, token={}, val={}".format(
        p.lineno, p.type, p.value))


log = logging.getLogger('ply')
_parser = yacc.yacc(errorlog=log)


def parse(input_str):
    global _defined_symbols
    _defined_symbols = set()
    return _parser.parse(input_str), _defined_symbols
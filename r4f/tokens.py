from ply import lex
import logging

reserved = {
    'require': 'REQUIRE',
    'if': 'IF',
    'while': 'WHILE',
    'do': 'DO',
    'stop': 'STOP',
    'def': 'DEF',
    'for': 'FOR',
    'to': 'TO',
    'with': 'WITH',
    'step': 'STEP',
}

tokens = [
    'DOT',
    'COLON',
    'COMMA',
    'EQ',
    'GT',
    'LT',
    'GE',
    'LE',
    'NE',
    'PLUS',
    'MINUS',
    'MUL',
    'DIV',
    'MOD',
    'ASSIGN',
    'LPAREN',
    'RPAREN',
    'INTEGER',
    'IDENT',
] + list(reserved.values())


t_DOT     = r"\."
t_COMMA   = r","
t_COLON   = r":"
t_EQ      = r"=="
t_GT      = r">"
t_LT      = r"<"
t_GE      = r">="
t_LE      = r"<="
t_NE      = r"!="
t_PLUS    = r"\+"
t_MINUS   = r"-"
t_MUL     = r"\*"
t_DIV     = r"/"
t_MOD     = r"%"
t_ASSIGN  = r"="
t_LPAREN  = r"\("
t_RPAREN  = r"\)"
t_INTEGER = r"[1-9][0-9]*|0"


def t_IDENT(t):
    r"[_a-zA-Z][_0-9a-zA-Z]*"
    t.type = reserved.get(t.value, 'IDENT')
    return t


t_ignore_WHITESPACE = r"\s"
t_ignore_COMMENT    = r'"[^\n\r]*(?:\n|\r|\r\n)'


log = logging.getLogger('ply')
lexer = lex.lex(errorlog=log)
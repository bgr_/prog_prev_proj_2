import pytest
from ply import lex
from r4f import tokens


@pytest.fixture()
def lexer():
    return lex.lex(module=tokens)


@pytest.mark.parametrize('inp', ["", " ", "   ", "\n", "\r",
                                 "\r\n", "\n\r", "\t", "\n  \n   \n  "])
def test_empty(inp, lexer):
    lexer.input(inp)
    assert list(lexer) == []


def test_simple(lexer):
    lexer.input("def x = 3. \n x + 2")
    output = [(t.type, t.value) for t in lexer]
    expected = [
        ('DEF', 'def'), ('IDENT', 'x'), ('ASSIGN', '='), ('INTEGER', '3'),
        ('DOT', '.'), ('IDENT', 'x'), ('PLUS', '+'), ('INTEGER', '2')]
    assert output == expected


def test_example_code(lexer):
    lexer.input(
        """
        require random.
        require os path.

        def var = 1.
        var = 10.


        while var > 0  do
            def t = 13.
            r4f:out( var + t * 9 ).
            var = var - 1 .
            def my_random_Number_1 = random:randint(100).
            r4f : out (my_random_Number_1) .
        stop

        for abc = 3 to 8 with step 2 do
            if abc > var do
                t = 3.
            stop
        stop
        """)

    output = ', '.join(['{} {}'.format(t.type, t.value) for t in lexer])
    expected = ', '.join([
        'REQUIRE require, IDENT random, DOT .',
        'REQUIRE require, IDENT os, IDENT path, DOT .',
        'DEF def, IDENT var, ASSIGN =, INTEGER 1, DOT .',
        'IDENT var, ASSIGN =, INTEGER 10, DOT .',
        'WHILE while, IDENT var, GT >, INTEGER 0, DO do',
        'DEF def, IDENT t, ASSIGN =, INTEGER 13, DOT .',
        'IDENT r4f, COLON :, IDENT out, LPAREN (, IDENT var, PLUS +, IDENT t, MUL *, INTEGER 9, RPAREN ), DOT .',  # noqa
        'IDENT var, ASSIGN =, IDENT var, MINUS -, INTEGER 1, DOT .',
        'DEF def, IDENT my_random_Number_1, ASSIGN =, IDENT random, COLON :, IDENT randint, LPAREN (, INTEGER 100, RPAREN ), DOT .',  # noqa
        'IDENT r4f, COLON :, IDENT out, LPAREN (, IDENT my_random_Number_1, RPAREN ), DOT .',  # noqa
        'STOP stop',
        'FOR for, IDENT abc, ASSIGN =, INTEGER 3, TO to, INTEGER 8, WITH with, STEP step, INTEGER 2, DO do',  # noqa
        'IF if, IDENT abc, GT >, IDENT var, DO do',
        'IDENT t, ASSIGN =, INTEGER 3, DOT .',
        'STOP stop',
        'STOP stop',
    ])
    assert output == expected
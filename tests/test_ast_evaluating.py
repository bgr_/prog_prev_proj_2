import pytest
from r4f.parsing import parse


@pytest.mark.parametrize('inp', ["", " ", "   ", "\n", "\r",
                                 "\r\n", "\n\r", "\t", "\n  \n   \n  "])
def test_empty(inp):
    result, _ = parse(inp)
    env = result.eval()
    assert len(env) == 0


@pytest.mark.parametrize('inp, expected', [
    ("def x = 3.",                              {'x': 3}),
    ("def y = 2 + 3.",                          {'y': 5}),
    ("def abc_1=2+3 .",                         {'abc_1': 5}),
    ("def a = 2 * 3 + 4 * 5.",                  {'a': 26}),
    ("def b = 2 * (3 + 4) * 5.",                {'b': 70}),
    ("def a = 3. def b = 4 * a.def c=3*a+2*b.", {'a': 3, 'b': 12, 'c': 33}),
])
def test_define_assign(inp, expected):
    result, _ = parse(inp)
    env = result.eval()
    assert env == expected


def test_while():
    result, _ = parse(
        """
        def z = 0.
        def i = 5.
        while i >= 3 do
            z = z + 10.
            i = i - 1.
        stop
        """)
    env = result.eval()
    assert env == {'z': 30, 'i': 2}


def test_if_gt_ge_lt_le_eq_ne():
    result, _ = parse(
        """
        def x = 89.
        def y = 89.
        if x > y  do def gt = 1. stop
        if x == y do def ge = 1. stop
        if x < y  do def lt = 1. stop
        if x <= y do def le = 1. stop
        if x == y do def eq = 1. stop
        if x != y do def ne = 1. stop
        """)
    env = result.eval()
    assert env == {'x': 89, 'y': 89, 'ge': 1, 'le': 1, 'eq': 1}


def test_for():
    result, _ = parse(
        """
        def i = 3.
        def x = 0.
        for i = 1 to 11 with step 1 do  " sum 1-10 == 55
            x = x + i.
        stop
        def z = x + i.  " should be 58 at this point
        for a = 0 to 10 with step 3 do
            z = z + a.  " will add 3 + 6 + 9 == 58 + 18 == 76
        stop
        """)
    env = result.eval()
    assert env == {'i': 3, 'x': 55, 'z': 76}


def test_namespace_reference_number():
    result, _ = parse(
        """
        require sys.
        require r4f.
        def mu = sys:maxunicode.
        def ver = r4f:VERSION.
        """)
    env = result.eval()
    assert env == {'mu': 1114111, 'ver': 42}


def test_function_call_simple():
    result, _ = parse(
        """
        require r4f.
        def nine = r4f:pow(3, 2).
        """)
    env = result.eval()
    assert env['nine'] == 9


def test_function_call_complex():
    result, _ = parse(
        """
        require r4f.
        def two = 2. def one = 1.
        def eighteen = r4f:pow(3, 2) + r4f:pow(
            one + two, r4f:VERSION - r4f:pow(3+3, one+one) - 4).
        """)
    env = result.eval()
    assert env['eighteen'] == 18


def test_function_call_as_statement():
    result, _ = parse(
        """
        require r4f.
        r4f:pow(3, 2).
        r4f:out(3).
        r4f:crlf().
        """)
    result.eval()  # just check that there are no errors
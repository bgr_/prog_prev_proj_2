import pytest
from r4f.parsing import parse
from r4f.ast import Assignment, ExprAdd, ExprSub, ExprMul, ExprDiv, Integer, \
    Reference, NSReference, Require, While, ExprGT, If, For, FunctionCall


@pytest.mark.parametrize('inp', ["", " ", "   ", "\n", "\r",
                                 "\r\n", "\n\r", "\t", "\n  \n   \n  "])
def test_empty(inp):
    result, defined_symbols = parse(inp)
    assert result.statements == []
    assert defined_symbols == set()


@pytest.mark.parametrize('inp, exp, names', [
    ("def x = 3.", [
        Assignment('x', Integer(3))
    ], ['x']),

    ("def x=45. def abc=2+x .", [
        Assignment('x', Integer(45)),
        Assignment('abc', ExprAdd(Integer(2), Reference('x'))),
    ], ['x', 'abc']),

    ("def abc = 0. def my_v4riable_123 = 2 * (abc + 4) / 5.", [
        Assignment('abc', Integer(0)),
        Assignment('my_v4riable_123',
                   ExprDiv(
                       ExprMul(
                           Integer(2),
                           ExprAdd(Reference('abc'), Integer(4))
                       ),
                       Integer(5)
                   )),
    ], ['abc', 'my_v4riable_123']),

    ("def xx = 45. def a = xx * 2 + 3 / xx.", [
        Assignment('xx', Integer(45)),
        Assignment('a',
                   ExprAdd(
                       ExprMul(Reference('xx'), Integer(2)),
                       ExprDiv(Integer(3), Reference('xx'))
                   )),
    ], ['xx', 'a'])
])
def test_define(inp, exp, names):
    result, defined_symbols = parse(inp)
    assert result.statements == exp
    assert defined_symbols == set(names)


def test_assign_raises_when_not_already_defined():
    with pytest.raises(ReferenceError) as e:
        parse("abc_9 = 4.")
    assert 'must be defined first' in str(e.value)


@pytest.mark.parametrize('inp', [
    "def x = 3. x = x * undef_x + 2.",
    "def x = 2. def y = 6. def z = undef_x / x.",
])
def test_define_and_assign_raise_on_undefined_reference(inp):
    with pytest.raises(ReferenceError) as e:
        parse(inp)
    assert "Undefined symbol 'undef_x'" in str(e.value)


def test_assign():
    result, defined_symbols = parse("def ta = 1.ta=2+ta .")
    assert result.statements == [
        Assignment('ta', Integer(1)),
        Assignment('ta', ExprAdd(Integer(2), Reference('ta'))),
    ]
    assert defined_symbols == set(['ta'])


@pytest.mark.parametrize('inp, exp', [
    ("require r4f.", [
        Require(['r4f']),
    ]),

    ("require os path .", [
        Require(['os', 'path']),
    ]),

    ("require r4f. require  os  path.", [
        Require(['r4f']),
        Require(['os', 'path']),
    ])
])
def test_require(inp, exp):
    result, _ = parse(inp)
    assert result.statements == exp


@pytest.mark.parametrize('inp', [
    "require r4g.",
    "require syss.",
    "require os pat.",
])
def test_require_raises_for_invalid_modules(inp):
    with pytest.raises(ImportError) as e:
        parse(inp)
    assert "Can't import" in str(e.value)


def test_while():
    result, defined_symbols = parse(
        """
        def z = 0.
        def i = 5.
        while i > 3 do
            z = z + 10.
            i = i - 1.
        stop
        """)
    assert result.statements == [
        Assignment('z', Integer(0)),
        Assignment('i', Integer(5)),
        While(
            condition_expression=ExprGT(Reference('i'), Integer(3)),
            statements=[
                Assignment('z', ExprAdd(Reference('z'), Integer(10))),
                Assignment('i', ExprSub(Reference('i'), Integer(1))),
            ]
        )
    ]
    assert defined_symbols == set(['z', 'i'])


def test_if():
    result, defined_symbols = parse(
        """
        def z = 0.
        def i = 5.
        if i > 3 do
            z = z + 10.
            i = i - 1.
            def x = 23.
        stop
        """)
    assert result.statements == [
        Assignment('z', Integer(0)),
        Assignment('i', Integer(5)),
        If(
            condition_expression=ExprGT(Reference('i'), Integer(3)),
            statements=[
                Assignment('z', ExprAdd(Reference('z'), Integer(10))),
                Assignment('i', ExprSub(Reference('i'), Integer(1))),
                Assignment('x', Integer(23)),
            ]
        )
    ]
    assert defined_symbols == set(['z', 'i', 'x'])


def test_for():
    result, defined_symbols = parse(
        """
        def x = 5.
        for i = 4 to 78 with step 2 do
            x = x + i.
        stop
        """)
    assert result.statements == [
        Assignment('x', Integer(5)),
        For(varname='i', start=4, stop=78, step=2, statements=[
            Assignment('x', ExprAdd(Reference('x'), Reference('i'))),
        ])
    ]
    assert defined_symbols == set(['x'])


def test_for_override_existing_var():
    result, defined_symbols = parse(
        """
        def i = 7.
        def x = 5.
        for i = 4 to 78 with step 2 do
            x = x + i.
        stop
        """)
    assert result.statements == [
        Assignment('i', Integer(7)),
        Assignment('x', Integer(5)),
        For(varname='i', start=4, stop=78, step=2, statements=[
            Assignment('x', ExprAdd(Reference('x'), Reference('i'))),
        ])
    ]
    assert defined_symbols == set(['x', 'i'])


@pytest.mark.parametrize('inp, exp, names', [
    ("require r4f. def i = r4f : out.", [
        Require(['r4f']), Assignment('i', NSReference(['r4f', 'out']))
    ], ['i', 'r4f.builtins']),
    ("require sys. def i = sys:maxunicode.", [
        Require(['sys']), Assignment('i', NSReference(['sys', 'maxunicode']))
    ], ['i', 'sys']),
    ("require os path. def i = os:path:dirname .", [
        Require(['os', 'path']),
        Assignment('i', NSReference(['os', 'path', 'dirname']))
    ], ['i', 'os.path']),
])
def test_namespace_reference(inp, exp, names):
    result, defined_symbols = parse(inp)
    assert result.statements == exp
    assert defined_symbols == set(names)


@pytest.mark.parametrize('inp', [
    "require r4f. def i = r4f : outt.",
    "require sys. def i = sys:maxunicod.",
    "require os path. def i = os:path:dirnam .",
])
def test_namespace_reference_raises_for_invalid_references_in_module(inp):
    with pytest.raises(ReferenceError) as e:
        parse(inp)
    assert 'not found in module' in str(e.value)


@pytest.mark.parametrize('inp', [
    "def i = r4f : out.",
    "def i = sys:maxunicode.",
    "def i = os:path:dirname .",
])
def test_namespace_reference_raises_for_unimported_module(inp):
    with pytest.raises(ReferenceError) as e:
        parse(inp)
    assert "Missing 'require' for" in str(e.value)


def test_function_call():
    result, defined_symbols = parse(
        """
        require r4f.
        def nine = r4f:pow(3, 2).
        r4f:out(nine).
        r4f:crlf().
        """)
    assert result.statements == [
        Require(['r4f']),
        Assignment('nine', FunctionCall(
            ref=NSReference(['r4f', 'pow']),
            args=[Integer(3), Integer(2)])
        ),
        FunctionCall(
            ref=NSReference(['r4f', 'out']),
            args=[Reference('nine')],
        ),
        FunctionCall(
            ref=NSReference(['r4f', 'crlf']),
            args=[],
        )
    ]
    assert defined_symbols == {'nine', 'r4f.builtins'}